#!/bin/bash          
./bootstrap

#generic configuration
./configure --enable-sii-assign --disable-8139too --enable-hrtimer --enable-cycles 

#configuration for inter 1000 pro
#./configure --enable-sii-assign --disable-8139too --enable-hrtimer --enable-cycles --enable-e1000e

sudo /opt/etherlab/etc/init.d/ethercat stop
make clean
make all modules
sudo make modules_install install

#adding ethercat library path to ldconfig 
sudo rm /etc/ld.so.conf.d/libethercat.conf
sudo touch /etc/ld.so.conf.d/libethercat.conf
echo "/opt/etherlab/lib/" | sudo tee -a /etc/ld.so.conf.d/libethercat.conf
sudo ldconfig

sudo depmod
sudo /opt/etherlab/etc/init.d/ethercat start

#optional disable etherlab module to load on start
sudo rm /etc/modprobe.d/blacklist-ecmaster.conf
sudo touch /etc/modprobe.d/blacklist-ecmaster.conf
echo "blacklist ec_master" | sudo tee -a /etc/modprobe.d/blacklist-ecmaster.conf
sudo update-initramfs
